import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DrinkWaterComponent } from './drink-water/drink-water.component';
import { PlanePathComponent } from './plane-path/plane-path.component';

@NgModule({
  declarations: [
    AppComponent,
    DrinkWaterComponent,
    PlanePathComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
