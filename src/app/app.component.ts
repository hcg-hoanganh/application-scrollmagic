import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
<!--    <app-drink-water></app-drink-water>-->
<app-plane-path></app-plane-path>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
