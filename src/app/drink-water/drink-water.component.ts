import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Controller, Scene} from 'scrollmagic';

@Component({
  selector: 'app-drink-water',
  template: `
    <header class="header section">
      <div class="center">&darr;</div>
    </header>

    <section class="scene section" #sticky>
      <div class="viewer"></div>
    </section>

    <section class="section">
      <div class="center">The End</div>
    </section>

  `,
  styleUrls: ['./drink-water.component.scss']
})
export class DrinkWaterComponent implements AfterViewInit {

  @ViewChild('sticky', {static: true}) stickyEle: ElementRef;

  ngAfterViewInit(): void {
    const nativeEle = this.stickyEle.nativeElement;

    const viewer = document.querySelector('.viewer'),
      frame_count = 9,
      offset_value = 100;

// init controller
    const controller = new Controller({
      globalSceneOptions: {
        triggerHook: 0,
        reverse: true
      }
    });

// build pinned scene
    new Scene({
      triggerElement: nativeEle,
      duration: (frame_count * offset_value) + 'px',
      reverse: true
    })
      .setPin(nativeEle)
      .addTo(controller);

// build step frame scene
    for (let i = 1, l = frame_count; i <= l; i++) {
      new Scene({
        triggerElement: nativeEle,
        offset: i * offset_value
      })
        .setClassToggle(viewer, 'frame' + i)
        .addTo(controller);
    }
  }

}
