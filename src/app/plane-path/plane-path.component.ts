import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {Controller, Scene} from 'scrollmagic';

@Component({
  selector: 'app-plane-path',
  template:
    `
      <header class="header section">
        <div class="center">&darr;</div>
      </header>


      <div
        class="container"
        #sticky>
        <div class="plane-path"

             [ngStyle]="{backgroundImage:'url(./assets/plane-path.png)'}">
          <img class="plan viewer" src="../../assets/plane.png" alt="">
        </div>


      </div>
      <section class="section">
        <div class="center">The End</div>
      </section>
    `,
  styleUrls: ['./plane-path.component.scss']
})
export class PlanePathComponent implements AfterViewInit {

  @ViewChild('sticky', {static: true}) stickyEle: ElementRef;

  ngAfterViewInit(): void {
    const nativeEle = this.stickyEle.nativeElement;

    const viewer = document.querySelector('.viewer'),
      frame_count = 40,
      offset_value = 100;

// init controller
    const controller = new Controller({
      globalSceneOptions: {
        triggerHook: 0,
        reverse: true
      }
    });

// build pinned scene
    new Scene({
      triggerElement: nativeEle,
      duration: (frame_count * offset_value) + 'px',
      reverse: true
    })
      .setPin(nativeEle)
      .addTo(controller);

// build step frame scene
    for (let i = 1, l = frame_count; i <= l; i++) {
      new Scene({
        triggerElement: nativeEle,
        offset: i * offset_value
      })
        .setClassToggle(viewer, 'frame' + i)
        .addTo(controller);
    }
  }

}
